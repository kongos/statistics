# read all csv data
library(ggplot2)
library(gridExtra)
partdata <- read.csv("participantData.csv")
tlxgaze <-read.csv("tlxgaze.csv")
tlxgesture <-read.csv("tlxgesture.csv")
tlxpush <-read.csv("tlxpush.csv")
tlxgrab <-read.csv("tlxgrab.csv")
pushMeasure <- read.csv("pushMeasures.csv")
grabMeasure <- read.csv("grabMeasures.csv")
gestureMeasure <- read.csv("gestureMeasures.csv")
gazeMeasure <- read.csv("gazeMeasures.csv")
tlxweight <-read.csv("TLXWEIGHT.csv")

hypotesH01 <-function(){
  # create character vectors to hold strings instead of true false
  pushVSGrab <- c("Push", "Grab")
  
  nogamerVSgamer <- c("Not a gamer", "Gamer")
  print("Hypotes H01 - Gamer experience Whether TP is a gamer or not should not affect experienced mental workload")
  print("t-test for all mental demand vs nongamer/gamer")
  print(t.test((tlxgrab$Mental.Demand+tlxgaze$Mental.Demand+tlxgesture$Mental.Demand + tlxpush$Mental.Demand)/4  ~ partdata$Gamer.))
 xy <- data.frame((tlxgrab$Mental.Demand+tlxgaze$Mental.Demand+tlxgesture$Mental.Demand + tlxpush$Mental.Demand)/4 , partdata$Gamer)
 nasaTlXValues <-  (tlxgrab$Mental.Demand+tlxgaze$Mental.Demand+tlxgesture$Mental.Demand + tlxpush$Mental.Demand)/4
 Gamer <- partdata$Gamer.
 png("hyp01.png",height=400, width=400)
 
 ggplot(xy, aes(Gamer, nasaTlXValues)) + 
    geom_boxplot(aes(fill = Gamer)) +
    theme(legend.position = "none") + ylab("TLX Mental Demand") + xlab("") +
   scale_x_discrete(labels =nogamerVSgamer) 
 dev.off()
  print("t-test between grab mental and Not a gamer vs Gamer")
  print(t.test(tlxgrab$Mental.Demand  ~ partdata$Gamer.))
 xy<-data.frame(tlxgrab$Mental.Demand, partdata$Gamer.)
 TLXMentalDemand <- tlxgrab$Mental.Demand
ggplot(xy, aes(Gamer, TLXMentalDemand)) + 
   geom_boxplot(aes(fill = Gamer)) +
   theme(legend.position = "none") + ylab("TLX Mental Demand") + xlab("") +
  scale_x_discrete(labels =nogamerVSgamer) 
         
           
  print("t-test between push mental demand and Not a gamer vs Gamer")
  print(t.test(tlxpush$Mental.Demand ~ partdata$Gamer.))
xy<-data.frame(tlxpush$Mental.Demand, partdata$Gamer.)
TLXMentalDemand <- tlxpush$Mental.Demand
ggplot(xy, aes(Gamer, TLXMentalDemand)) + 
  geom_boxplot(aes(fill = Gamer)) +
  theme(legend.position = "none") +ylab("TLX Mental Demand") + xlab("") +
  scale_x_discrete(labels =nogamerVSgamer) 
  
  print("t-test between gaze mental and Not a gamer vs Gamer")
  print(t.test(tlxgaze$Mental.Demand ~ partdata$Gamer.))
xy<-data.frame(tlxgaze$Mental.Demand, partdata$Gamer.)
TLXMentalDemand <- tlxgaze$Mental.Demand
ggplot(xy, aes(Gamer, TLXMentalDemand)) + 
  geom_boxplot(aes(fill = Gamer)) +
  theme(legend.position = "none") + ylab("TLX Mental Demand") + xlab("") +
  scale_x_discrete(labels =nogamerVSgamer) 
   
  print("t-test between gesture mental and Not a gamer vs Gamer")
  print(t.test(tlxgesture$Mental.Demand ~ partdata$Gamer.))
xy<-data.frame(tlxgesture$Mental.Demand, partdata$Gamer.)
lab<-c("Not a Gamer", "Gamer")
TLXMentalDemand <- tlxgesture$Mental.Demand
ggplot(xy, aes(Gamer, TLXMentalDemand)) + 
  geom_boxplot(aes(fill = Gamer)) +
  theme(legend.position = "none") +ylab("TLX Mental Demand") + xlab("") +
  scale_x_discrete(labels =nogamerVSgamer) 
  
  
}
hypotesH9 <-function(){
  nogamerVSgamer <- c("Not a gamer", "Gamer")
  print("Hypotes H9 - gamers are quicker")
  print("t-test between push time and Not a gamer vs Gamer")
  print(t.test(pushMeasure$Time.in.seconds ~ partdata$Gamer.))
  boxplot(pushMeasure$Time.in.seconds ~ partdata$Gamer., type = "o", pch = 21:23, col = 3:6, bg = 2:5,
          main = "Time for Push", names=nogamerVSgamer, ylab = "Time in s")
  
  print("t-test between grab time and Not a gamer vs Gamer")
  print(t.test(grabMeasure$Time.in.seconds ~ partdata$Gamer.))
  boxplot(grabMeasure$Time.in.seconds ~ partdata$Gamer., type = "o", pch = 21:23, col = 3:6, bg = 2:5,
          main = "Time for Grab", names=nogamerVSgamer, ylab = "Time in s")
  print("t-test between gesture time and Not a gamer vs Gamer")
  print(t.test(gestureMeasure$Time.in.seconds ~ partdata$Gamer.))
  boxplot(gestureMeasure$Time.in.seconds ~ partdata$Gamer., type = "o", pch = 21:23, col = 3:6, bg = 2:5,
          main = "Time for Gesture", names=nogamerVSgamer, ylab = "Time in s")
  print("t-test between gaze time and Not a gamer vs Gamer")
  print(t.test(gazeMeasure$Time.in.seconds ~ partdata$Gamer.))
  boxplot(gazeMeasure$Time.in.seconds ~ partdata$Gamer., type = "o", pch = 21:23, col = 3:6, bg = 2:5,
          main = "Time for Gaze", names=nogamerVSgamer, ylab = "Time in s")
}
hypotesH3 <- function(){
  print("Hypotes H3 gaze will be quicker then gesture")
  print(t.test(gazeMeasure$Time.in.seconds, gestureMeasure$Time.in.seconds))
  xy<-data.frame(gazeMeasure$Time.in.seconds, gestureMeasure$Time.in.seconds)
  colnames <-c("Gaze", "Gesture")
  png("gazeVsGestureTime.png",height=400, width=400)
  
  ggplot(data = melt(xy), aes(x=variable, y=value)) + geom_boxplot(aes(fill=variable)) + 
    theme(legend.position = "none") +ylab("Time in s") + xlab("") +
    scale_x_discrete(labels =colnames) 
  dev.off()
}
hypotesH4 <-function(){
  print("Hypotes H4 - push will be quicker then grab")
  print(t.test(pushMeasure$Time.in.seconds, grabMeasure$Time.in.seconds))
  xy<-data.frame(pushMeasure$Time.in.seconds, grabMeasure$Time.in.seconds)
  colnames(xy) <-c("Push", "Grab")
  png("pushVsGrabTime.png",height=400, width=400)
  
  ggplot(data = melt(xy), aes(x=variable, y=value)) + geom_boxplot(aes(fill=variable)) + 
    theme(legend.position = "none") +ylab("Time in s") + xlab("") 
  dev.off()
}
hypotesH6 <-function(){
  print("Hypotes H6 - Gaze will be Less physical demanding than gesture")
  print(t.test(tlxgaze$Physical.Demand, tlxgesture$Physical.Demand))
  xy<-data.frame(tlxgaze$Physical.Demand, tlxgesture$Physical.Demand)
  colnames(xy) <-  c("Gaze", "Gesture")
  png("gazeVsGesturePhyD.png",height=400, width=400)
  
  ggplot(data = melt(xy), aes(x=variable, y=value)) + geom_boxplot(aes(fill=variable))+ 
    theme(legend.position = "none") +ylab("TLX Value") + xlab("") 
  dev.off()
}
hypotesH7 <-function(){
  print("Hypotes H7 - Push will be less physical demanding then Grab")
  print(t.test(tlxpush$Physical.Demand, tlxgrab$Physical.Demand))
  xy<-data.frame(tlxpush$Physical.Demand, tlxgrab$Physical.Demand)
  colnames(xy) <-c("Push", "Grab")
  png("pushVsGrabPhyD.png",height=400, width=400)
  
  ggplot(data = melt(xy), aes(x=variable, y=value)) + geom_boxplot(aes(fill=variable))+ 
    theme(legend.position = "none") +ylab("TLX Value") + xlab("") 
  dev.off()
}
hypotesH8 <-function(){
  qualative <- read.csv("qualativeQuantifiedwithoutmissingaudio.csv")
  partdatawithoutsound <- read.csv("participantDatawithoutmissingaudio.csv")
  print("Hypotes H8 - Gamers will have higher preference for Gaze")
  print("Using a subset of TPs since some TPs are missing sound")
  print(t.test(qualative$PreferGaze, partdata$Gamer.))
  
}
hypotesH2 <-function(){
  print("Hypotes H2 - The less percieved frustration the higher preference")
  qualative <- read.csv("qualativeQuantifiedwithoutmissingaudio.csv")
  gazeTlxNoSound <- read.csv("tlxgazeNoSound.csv")
  grabTlxNoSound <- read.csv("tlxgrabNoSound.csv")
  pushTlxNoSound <- read.csv("tlxpushNoSound.csv")
  gestTlxNoSound <- read.csv("tlxgestureNoSound.csv")
  print("Gaze Frustration vs Gesture/Gaze Preference")
  print(t.test(gazeTlxNoSound$Frustration ~qualative$PreferGaze))
  xy<-data.frame(gazeTlxNoSound$Frustration, qualative$PreferGaze)
  lab<-c("Prefer Gesture", "Prefer Gaze")
  
  ggplot(data=melt(xy), aes(qualative$PreferGaze, gazeTlxNoSound$Frustration)) + 
    geom_boxplot(aes(fill = qualative$PreferGaze)) +
    theme(legend.position = "none") +ylab("TLX Frustration") + xlab("") +
    scale_x_discrete(labels =lab) 
  
  qa <-0+qualative
  print("Corrolation between Gaze Frustration and if Gaze is prefered")
  print(cor.test(gazeTlxNoSound$Frustration , qa$PreferGaze))
  print("Grab Frustration vs Grab/Push Preference")
  print(t.test(grabTlxNoSound$Frustration ~qualative$PreferPush))
  xy<-data.frame(grabTlxNoSound$Frustration, qualative$PreferPush)
  lab<-c("Prefer Grab", "Prefer Push")
  ggplot(data=melt(xy), aes(qualative$PreferPush, grabTlxNoSound$Frustration)) + 
    geom_boxplot(aes(fill = qualative$PreferPush)) +
    theme(legend.position = "none") +ylab("TLX Frustration") + xlab("") +
    scale_x_discrete(labels =lab) 
  
  print("Corrolation between Grab Frustration and if grab is prefered")
  print(cor.test(grabTlxNoSound$Frustration , qa$PreferPush))
  print("Push Frustration vs Grab/Push Preference")
  print(t.test(pushTlxNoSound$Frustration ~qualative$PreferPush))
  xy<-data.frame(pushTlxNoSound$Frustration, qualative$PreferPush)
  lab<-c("Prefer Grab", "Prefer Push")
  ggplot(data=melt(xy), aes(qualative$PreferPush, pushTlxNoSound$Frustration)) + 
    geom_boxplot(aes(fill = qualative$PreferPush)) +
    theme(legend.position = "none") +ylab("TLX Frustration") + xlab("") +
    scale_x_discrete(labels =lab)
  
  print("Corrolation between Push Frustration and if push is prefered")
  print(cor.test(pushTlxNoSound$Frustration , qa$PreferPush))
  print("Gesture Frustration vs Gesture/Gaze Preference")
  print(t.test(gestTlxNoSound$Frustration ~qualative$PreferGaze))
  xy<-data.frame(gestTlxNoSound$Frustration, qualative$PreferGaze)
  lab<-c("Prefer Gesture", "Prefer Gaze")
  ggplot(data=melt(xy), aes(qualative$PreferGaze, gestTlxNoSound$Frustration)) + 
    geom_boxplot(aes(fill = qualative$PreferGaze)) +
    theme(legend.position = "none") +ylab("TLX Frustration") + xlab("") +
    scale_x_discrete(labels =lab)
  
  print("Corrolation between Gesture Frustration and if push is prefered")
  print(cor.test(gestTlxNoSound$Frustration , qa$PreferPush)) 
}
tlxgaze$WorkLoad <- NA
tlxgrab$WorkLoad <- NA
tlxpush$WorkLoad <- NA
tlxgrab$WorkLoad <- NA
for(i in 1:nrow(tlxgaze)){
  ##Workload för gaze
  tlxgaze$WorkLoad[i] <- ((tlxgaze$Mental.Demand[i]*tlxweight$Mental.Demand[1]) +
                            (tlxgaze$Physical.Demand[i]*tlxweight$Physical.Demand[1]) +
                            (tlxgaze$Temporal.Demand[i]*tlxweight$Temporal.Demand[1]) +
                            (tlxgaze$Performance[i]*tlxweight$Performance[1]) +
                            (tlxgaze$Effort[i]*tlxweight$Effort[1]) +
                            (tlxgaze$Frustration[i]*tlxweight$Frustration[1]))/15
  ##Workload för gesture
  tlxgesture$WorkLoad[i] <- ((tlxgesture$Mental.Demand[i]*tlxweight$Mental.Demand[2]) +
                               (tlxgesture$Physical.Demand[i]*tlxweight$Physical.Demand[2]) +
                               (tlxgesture$Temporal.Demand[i]*tlxweight$Temporal.Demand[2]) +
                               (tlxgesture$Performance[i]*tlxweight$Performance[2]) +
                               (tlxgesture$Effort[i]*tlxweight$Effort[2]) +
                               (tlxgesture$Frustration[i]*tlxweight$Frustration[2]))/15
  ##Workload för push
  tlxpush$WorkLoad[i] <- ((tlxgesture$Mental.Demand[i]*tlxweight$Mental.Demand[3]) +
                            (tlxpush$Physical.Demand[i]*tlxweight$Physical.Demand[3]) +
                            (tlxpush$Temporal.Demand[i]*tlxweight$Temporal.Demand[3]) +
                            (tlxpush$Performance[i]*tlxweight$Performance[3]) +
                            (tlxpush$Effort[i]*tlxweight$Effort[3]) +
                            (tlxpush$Frustration[i]*tlxweight$Frustration[3]))/15
  ##Workload för grab
  tlxgrab$WorkLoad[i] <- ((tlxgesture$Mental.Demand[i]*tlxweight$Mental.Demand[4]) +
                            (tlxgrab$Physical.Demand[i]*tlxweight$Physical.Demand[4]) +
                            (tlxgrab$Temporal.Demand[i]*tlxweight$Temporal.Demand[4]) +
                            (tlxgrab$Performance[i]*tlxweight$Performance[4]) +
                            (tlxgrab$Effort[i]*tlxweight$Effort[4]) +
                            (tlxgrab$Frustration[i]*tlxweight$Frustration[4]))/15
  
}
plotWorkloadDensity <-function(){
  print("Prints the workload density for all gestures")
  #Plot the density of the Gaze Workload
  plot(density(tlxgaze$WorkLoad), main="Density of Gaze Workload")
  rug(tlxgaze$WorkLoad)
  #Plot the density of the Grab Workload
  plot(density(tlxgrab$WorkLoad), main="Density of Grab Workload")
  rug(tlxgrab$WorkLoad)
  #Plot the density of the Push Workload
  plot(density(tlxpush$WorkLoad), main="Density of Push Workload")
  rug(tlxpush$WorkLoad)
  #Plot the density of the Gesture Workload
  plot(density(tlxgesture$WorkLoad), main="Density of Gesture Workload")
  rug(tlxgesture$WorkLoad)
}
hypotesH03 <- function() {
  print("Hypotes H03 - Gender will not affect the time to finish a task")
  genders<-rep(NA, length(partdata$Gender))
  for(i in 1:length(partdata$Gender)){
    if(partdata$Gender[i]=="Female"){
      genders[i]=0
    }else if(partdata$Gender[i]=="female"){
      genders[i]=0
    }else if(partdata$Gender[i]=="Male"){
      genders[i]=1
    }else if(partdata$Gender[i]=="male"){
      genders[i]=1
    }
  }
  
  print("Test för Gaze")      
  print(cor.test(genders, gazeMeasure$Time.in.seconds))
  print("Test för Gesture")
  print(cor.test(genders, gestureMeasure$Time.in.seconds))
  print("Test för Push")
  print(cor.test(genders, pushMeasure$Time.in.seconds))
  print("Test för Grab")
  print(cor.test(genders, grabMeasure$Time.in.seconds))
  
}
hypotesH04 <-function(){
  print("Hypotes H04 - There will be no corrolation between the time to finish a task and the percieved cognitive workload")
  # print("Test för H04(Gaze)")
  # print(t.test(gazeMeasure$Time.in.seconds, tlxgaze$WorkLoad))
  # print("Test för H04(Gesture)")
  # print(t.test(gestureMeasure$Time.in.seconds, tlxgesture$WorkLoad))
  # print("Test för H04(Push)")
  # print(t.test(pushMeasure$Time.in.seconds, tlxpush$WorkLoad))
  # print("Test för H04(Grab)")
  # print(t.test(grabMeasure$Time.in.seconds, tlxgrab$WorkLoad))
  
  print("Correlation mellan Tid och Kog.Workload")
  print("Correlation för Gaze")
  print(cor.test(gazeMeasure$Time.in.seconds, tlxgaze$WorkLoad))
  print("Correlation för Gesture")
  print(cor.test(gestureMeasure$Time.in.seconds, tlxgesture$WorkLoad))
  print("Correlation för Push")
  print(cor.test(pushMeasure$Time.in.seconds, tlxpush$WorkLoad))
  print("Correlation för Grab")
  print(cor.test(grabMeasure$Time.in.seconds, tlxgrab$WorkLoad))
  
}
hypotesH02 <- function(){
  print("Hypotes H02 - Age should not affect the time to finish the task")
  print("Correlation between Time and Age")
  print("Gaze")
  print(cor.test(gazeMeasure$Time.in.seconds, partdata$Age))
  print("Gesture")
  print(cor.test(gestureMeasure$Time.in.seconds, partdata$Age))
  print("Push")
  print(cor.test(pushMeasure$Time.in.seconds, partdata$Age))
  print("Grab")
  print(cor.test(grabMeasure$Time.in.seconds, partdata$Age))
}

hypotesH10 <-function(){
  print("Hypotes H10 - Younger persons are less paranoid")
  qualativeNo <- read.csv("qualativeQuantifiedwithoutmissingaudio.csv")
  qualative <-read.csv("qualativeQuantified.csv")
  qualative <- na.omit(qualative)
  partDataNo <- read.csv("participantDatawithoutmissingaudio.csv")
  boxplot(partDataNo$Age ~ qualativeNo$Paranoid, type = "o", pch = 21:23, col = 3:6, bg = 2:5,
          main = "Paranoia Over ages", names=c("Not Paranoid", "Neither", "Paranoid"), ylab = "Age")
  plot(density(qualative$Paranoid), main="Paranoia 1=Not 2=Ambivalent 3=High")
}
